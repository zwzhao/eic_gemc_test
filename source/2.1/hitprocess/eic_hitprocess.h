#ifndef EIC_HITPROCESS_HH
#define EIC_HITPROCESS_HH

#include "HitProcess.h"
#include "HitProcess_MapRegister.h"

#include "eic_dirc_hitprocess.h"
#include "eic_ec_hitprocess.h"
#include "eic_preshower_hitprocess.h"
#include "eic_rich_hitprocess.h"

//   This function allows us to add in our own hit processor for SoLID

void eic_hitprocess( map<string, HitProcess_Factory> &hitMap ){
    hitMap["eic_dirc"]  = &eic_dirc_HitProcess::createHitClass;
    hitMap["eic_ec"]  = &eic_ec_HitProcess::createHitClass;
    hitMap["eic_preshower"]  = &eic_preshower_HitProcess::createHitClass;    
    hitMap["eic_rich"]  = &eic_rich_HitProcess::createHitClass;        
    return;
}

#endif//EIC_HITPROCESS_HH
